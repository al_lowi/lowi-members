Template.memberNew.events "submit form": (e) ->
  e.preventDefault()
  properties =
    name: $(e.target).find("[name=name]").val()
    email: $(e.target).find("[name=email]").val()
    nickname: $(e.target).find("[name=nickname]").val()
    roles: $('select#roles').val()
    hostname: window.location.hostname

  shareDialogInfo =
    template: Template.spinner
    title: "Wait"
    modalDialogClass: "wait-dialog" #optional
    modalBodyClass: "share-modal-body" #optional
    modalFooterClass: "share-modal-footer" #optional
    removeOnHide: true #optional. If this is true, modal will be removed from DOM upon hiding
    buttons: {}

  rd = ReactiveModal.initDialog(shareDialogInfo)
  rd.show()

  Meteor.call "memberNew", properties, (error, id) ->
    rd.hide()
    if error then Errors.throwError error.reason else Router.go "members"
    return

  return