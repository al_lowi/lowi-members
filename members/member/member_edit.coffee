Template.memberEdit.events
  "submit form": (e) ->
    e.preventDefault()
    properties =
      name: $(e.target).find("[name=name]").val()
      gender: $(e.target).find("[name=gender]:checked").val()
      email: $(e.target).find("[name=email]").val()
      memberId: @_id
      roles: $('select#roles').val()
      nickname: $(e.target).find("[name=nickname]").val()

    Meteor.call "memberUpdate", properties, (error, id) ->
      if error then Errors.throwError error.reason else Router.go "members"
      return

  "click .delete": (e) ->
    e.preventDefault()
    if confirm("Delete this member?")
      Meteor.call "memberDelete", @_id, (error, id) ->
        if error then Errors.throwError error.reason else Router.go "members"
      return
    return

Template.memberEdit.rendered = ->
  $( "##{@data.gender}").prop('checked', true)
  $('#roles').multiSelect('select', @data.profile.roles);