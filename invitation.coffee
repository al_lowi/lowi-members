Template.invitation.events
  "click #join" : (ev) ->
    memberId = @memberId
    email=@email
    password=$('input[name=password]').val()

    Meteor.call "invitationAccepted", @_id, $('input[name=password]').val(), $('input[name=password2]').val(), (error, id) ->
      if error
        Errors.throwError error.reason
      else
        Meteor.setTimeout( () ->
          Meteor.loginWithPassword(email, password, (err)->
            console.log err
            if err
              console.log email
              console.log password
              console.log memberId
              Errors.throwError "We were unable to log you in"
            else
              Router.go "dashboard"
          )
        , 2000)

    return false