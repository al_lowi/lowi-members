filterByOrg = (meteor, collection, userId, strict) ->
  if userId
    user = Meteor.users.findOne(userId)
    collection.find({orgId: user.profile.orgId})
  else if not strict
    collection.find()
  else
    meteor.ready()


filterByOrg = (meteor, collection, userId, strict, options) ->
  if userId
    user = Meteor.users.findOne(userId)
    collection.find({orgId: user.profile.orgId}, options)
  else if not strict
    collection.find()
  else
    meteor.ready()

Meteor.publish "members", (limit) ->
  filterByOrg(this, Members.collection, @userId, true, {limit: limit})

Meteor.publish "member", (memberId) ->
  user = Meteor.users.findOne(@userId)
  if user
    Members.collection.find({_id: memberId, orgId: user.profile.orgId})

Meteor.publish "invitation", (id) ->
  Members.invitations.find({_id: id})

Meteor.methods
  memberAutocomplete: (name) ->
    search = {$regex: new RegExp(name, 'i')}
    if Meteor.user()
      user = Meteor.users.findOne(Meteor.user()._id)
      return Members.collection.find(
        $and:[
          $or: [{"profile.name":search}, {"profile.nickname":search}]
          orgId: user.profile.orgId
        ]
      ).fetch().map((member) -> {value: member.profile.name, gender:member.gender, memberId: member._id})
    else
      return []

  invitationAccepted: (invitationId, password, password2) ->
    throw new Meteor.Error(422, "Please type in a password")  unless password
    throw new Meteor.Error(422, "The two passwords are different")  unless password is password2

    invitation = Members.invitations.findOne({_id: invitationId})
    throw new Meteor.Error(422, "Unknown invitation")  unless invitation

    memberId = invitation.memberId
    member = Members.collection.findOne({_id: memberId})
    throw new Meteor.Error(422, "Unknown member")  unless member

    accountId = Accounts.createUser
#      username: invitation.profile.nickname
      email: invitation.email
      profile: invitation.profile
      password: password
      verified: true

    Meteor.users.update(
      _id: accountId
    ,
      $set:
        username: invitation.email
        profile: invitation.profile
    )


    Members.collection.update({_id: memberId},
      $set:
        accountId: accountId
    )

    if member.profile.roles?.length > 0
      Roles.addUsersToRoles(accountId, member.profile.roles)

    invitationId
