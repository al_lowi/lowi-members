Package.describe({
  name: 'lowi-members',
  summary: ' Members handling are separated from the meteor authentication system',
  version: '1.0.0',
  git: 'https://al_lowi@bitbucket.org/al_lowi/lowi-members.git'
});

Package.onUse(function(api) {
  api.versionsFrom('1.0');
  api.use(['minimongo', 'mongo-livedata', 'templating', 'pahans:reactive-modal'], 'client');
  api.use("mquandalle:jade");
  api.use(["coffeescript", "mongo", "email", 'alanning:roles', 'sacha:spin']);
  api.addFiles(['members.js', 'members.coffee']);
  api.addFiles(['invitation.jade', 'invitation.coffee'], 'client');
  api.addFiles(['members/list/members.html', 'members/list/members.coffee', 'members/list/member.html'], 'client');
  api.addFiles(['members/member/eachProp.html', 'members/member/eachProp.coffee'], 'client');
  api.addFiles(['members/member/member_edit.html', 'members/member/member_edit.coffee', 'members/member/member_new.html', 'members/member/member_new.coffee'], 'client');
  api.addFiles(['server.coffee'], 'server');

  if (api.export) 
	api.export('Members');
});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('errors');
  api.addFiles('errors-tests.js');
});
