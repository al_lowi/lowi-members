purgeMember = (id, accountId) ->
  Members.collection.remove({_id: id})
  if accountId and Meteor.isServer
    Meteor.users.remove({_id: accountId})

sendEmail = (invitationId, email) ->
  Queue.add
    command:"console.log('running job'); Email.send({from:'alex@amically.com', to:#{email}, subject: 'You have been added as a member', text: 'Please register with this link: http://localhost:3000/invitation/#{invitationId}'});"
  Queue.run()

Meteor.methods
  memberUpdate: (att) ->
    user = Meteor.user()

    throw new Meteor.Error(401, "You need to login to post new member")  unless user
    throw new Meteor.Error(422, "Please fill in with the memberId")  unless att.memberId
    throw new Meteor.Error(422, "Please fill in with a name")  unless att.name

    member = Members.collection.findOne({_id: att.memberId})

    updatedInformation =
      email: att.email
      "profile.name": att.name
      "profile.nickname": att.nickname


    isAdmin = Roles.userIsInRole(user, ['admin'])

    if isAdmin
      updatedInformation["profile.roles"] = att.roles

    if member.accountId is user._id or isAdmin
      Members.collection.update(
        _id: att.memberId
      ,
        $set: updatedInformation
      )

    Meteor.history("updated member #{att.name}")

    if member.accountId and Meteor.isServer and isAdmin
      Roles.setUserRoles(member.accountId, att.roles or [])

  memberDelete: (memberId) ->
    user = Meteor.user()

    # ensure the user is logged in
    throw new Meteor.Error(401, "You need to login to post new member")  unless user

    member = Members.collection.findOne({_id: memberId})
    if member
      purgeMember(memberId, member.accountId)
      Meteor.history("deleted member #{member.profile.name}")

    return

  memberNew: (att) ->
    user = Meteor.user()

    # ensure the user is logged in
    throw new Meteor.Error(401, "You need to login to add new member")  unless user
    throw new Meteor.Error(422, "Please fill in with a name")  unless att.name
    orgId = user.profile.orgId

    memberId = Members.collection.insert
      email: att.email
      orgId: orgId
      profile:
        name: att.name
        nickname: att.nickname
        roles: att.roles

    Meteor.history("added member #{att.name}")

    if Meteor.isServer
      invitationId = Members.invitations.insert
        memberId: memberId
        email: att.email
        profile:
          name: att.name
          nickname: att.nickname
          roles: att.roles
          orgId: orgId

      if process.env.RATIONALK_CLIENT_URL
          clientURL=process.env.RATIONALK_CLIENT_URL
      else
          clientURL=process.env.ROOT_URL
      Email.send(
        from: "admin@rationalk.ch"
        to: att.email
        subject: "[rationalK] You have been added as a member"
        text: "Please register with this link: #{clientURL}invitation/#{invitationId}"
      )

    return memberId